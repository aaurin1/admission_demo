#!/bin/bash

if [ -e "demo-magic.sh" ]; then
    echo 'demo-magic.sh already exists' >&2
else
    curl -o "demo-magic.sh" "https://raw.githubusercontent.com/open-policy-agent/gatekeeper/master/third_party/demo-magic/demo-magic.sh"
fi

. ./demo-magic.sh

clear
###
#Classic ns
###
pe "cat classic/ns.yaml"

pe "kubectl apply -f classic/ns.yaml"

pe "kubectl apply -f classic/pod_root.yaml -n classic-ns"

pe "kubectl apply -f classic/pod_user.yaml -n classic-ns"

pe "kubectl get pod -w -n classic-ns"

pe "kubectl -n classic-ns exec -ti user-app -- id"

pe "kubectl -n classic-ns exec -ti default-app -- id"

pe "kubectl apply -f classic/service.yaml -n classic-ns"

pe "kubectl apply -f classic/service_default.yaml -n classic-ns"

pe "kubectl apply -f classic/ingress.yaml -n classic-ns"

pe "kubectl apply -f classic/ingress_default.yaml -n classic-ns"

pe "kubectl get ingress -w -n classic-ns"
####
## warn ns
####
pe "cat warn/ns.yaml"

pe "kubectl apply -f warn/ns.yaml"

pe "kubectl apply -f warn/pod.yaml -n baseline-ns"

pe "kubectl get pod -n baseline-ns"

pe "kubectl apply -f warn/service.yaml -n baseline-ns"

pe "kubectl apply -f warn/ingress_warn.yaml -n baseline-ns"

pe "kubectl get ingress -w -n baseline-ns"
####
## rextrict ns
####
pe "cat restricted/ns.yaml"

pe "kubectl apply -f restricted/ns.yaml"

pe "kubectl apply -f restricted/pod.yaml -n restricted-ns"

pe "kubectl get pod -n restricted-ns"

####
## show ns label
####
pe "kubectl get ns --show-labels"

p "THE END"

kubectl delete -f classic
kubectl delete -f warn
kubectl delete -f restricted/ns.yaml
